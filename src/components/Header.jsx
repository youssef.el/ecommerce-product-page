import React, { useState } from 'react'
import avatar from '../assets/images/image-avatar.png'
import {BsTrash3} from 'react-icons/bs'
import {AiOutlineClose} from 'react-icons/ai'
import image from '../assets/images/image-product-1-thumbnail.jpg'

const Header = (props) => {

  const [open, setOpen] = useState(false)

  return (
  <div>
    <div className="container navbar pt-6 pb-0 px-6 mx-auto flex items-center justify-between bg-base-100">
      <div className='flex items-center justify-between lg:space-x-5'>
        {/* mobile hamburger menu */}
        <div className="flex lg:hidden ">
          <button onClick={()=>setOpen(true)} className="btn btn-circle bg-white border-none text-black hover:bg-white">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-7 h-7 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
          </button>
        </div> 
        {/* mobile menu elements */}
        <div className={open ? 'fixed flex items-start flex-col top-0 left-0 h-full w-[60%] bg-white z-10 lg:hidden orders-1' :'hidden'}>
          <button onClick={()=>setOpen(false)} className='btn btn-ghost m-2 hover:bg-transparent hover:text-gray-300'><AiOutlineClose size={20} /></button>
          <a href="" className='btn  btn-outline border-none font-bold mt-2 ml-4 hover:bg-white hover:text-gray-300' >Collections</a>
          <a href="" className='btn  btn-outline border-none font-bold mt-2 ml-4 hover:bg-white hover:text-gray-300'>Men</a>
          <a href="" className='btn  btn-outline border-none font-bold mt-2 ml-4 hover:bg-white hover:text-gray-300'>Women</a>
          <a href="" className='btn  btn-outline border-none font-bold mt-2 ml-4 hover:bg-white hover:text-gray-300'>About</a>
          <a href="" className='btn  btn-outline border-none font-bold mt-2 ml-4 hover:bg-white hover:text-gray-300'>Contact</a>
        </div>

        {/* logo */}
        <div className="flex lg:pt-1 ">
          <a href='#' className="normal-case font-extrabold text-4xl bg-white border-none text-black hover:bg-white lg:pt-1 ">sneakers</a>
          {/* menu links */}
          <div className='hidden lg:flex ml-10 text-lg pt-2 '>
          <a href="" className='btn  btn-outline border-t-0 border-l-0 border-r-0 rounded-none border-transparent hover:bg-white hover:text-gray-300 hover:border-b-2 hover:border-b-red-600' >Collections</a>
          <a href="" className='btn  btn-outline border-t-0 border-l-0 border-r-0 rounded-none border-transparent hover:bg-white hover:text-gray-300 hover:border-b-2 hover:border-b-red-600'>Men</a>
          <a href="" className='btn  btn-outline border-t-0 border-l-0 border-r-0 rounded-none border-transparent hover:bg-white hover:text-gray-300 hover:border-b-2 hover:border-b-red-600'>Women</a>
          <a href="" className='btn  btn-outline border-t-0 border-l-0 border-r-0 rounded-none border-transparent hover:bg-white hover:text-gray-300 hover:border-b-2 hover:border-b-red-600'>About</a>
          <a href="" className='btn  btn-outline border-t-0 border-l-0 border-r-0 rounded-none border-transparent hover:bg-white hover:text-gray-300 hover:border-b-2 hover:border-b-red-600'>Contact</a>
          </div>
        </div> 
      </div> 
      {/* cart and avatar  */}
      <div className="flex items-center justify-between h-10 px-4 ">
      {/* cart  */}
        <div className="dropdown dropdown-end mr-4 lg:mt-0">
          <label tabIndex={0} className="btn btn-ghost border-none btn-circle">
            <div className="indicator">
              <svg xmlns="http://www.w3.org/2000/svg" className="w-7 lg:h-7 lg:w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" /></svg>
              {props.cartItems > 0 && <span className="inline-block badge md:badge-sm  indicator-item">{props.cartItems}</span>}
            </div>
          </label>
          <div tabIndex={0} className="mt-3 card card-compact dropdown-content w-80 bg-base-100 shadow">
            <div className="card-body">
              <h2 className='font-bold'>Cart</h2>
              <hr />
              {/* cart not empty */}
              {props.cartItems > 0  && <div className='flex items-center justify-between'>
                <img src={image} className='w-10 h-10 rounded-lg' />
                <div>
                  <h2>Full Limited Edition Sneakers</h2>
                  <p>{`$125.00 x ${props.cartItems}`}<span className='font-bold ml-2'>{`$${125*props.cartItems}.00`}</span>  </p>
                </div>
                <BsTrash3 className='cursor-pointer' onClick={props.ClearCount} size={20} />
              </div>}
              {props.cartItems > 0 && <div className="card-actions">
                <button className="btn bg-orange-500 text-white border-none drop-shadow-2xl hover:bg-orange-300 btn-block">Checkout</button>
              </div>}
              {props.cartItems === 0 && <p className='font-bold text-slate-500 p-10 text-center'>Your Cart is empty</p>}
            </div>
            
          </div>
        </div>
        {/* avatar logo */}
          <label tabIndex={0} className="btn border-none btn-circle bg-white hover:bg-white avatar lg:pl-6 w-10 lg:w-20">
            <div className="lg:w-10 lg:h-10 rounded-full">
              <img src={avatar} />
            </div>
          </label>
      </div>
    </div>
    <div className='hidden px-16 lg:block'>
      <hr />
    </div>

</div>
  )
}

export default Header
