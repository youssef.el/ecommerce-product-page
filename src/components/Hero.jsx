import React, { useState } from 'react'
import {AiOutlineMinus, AiOutlinePlus, AiOutlineShoppingCart} from 'react-icons/ai'
import {BiChevronLeft, BiChevronRight} from 'react-icons/bi'
import image1 from '../assets/images/image-product-1.jpg'
import image1th from '../assets/images/image-product-1-thumbnail.jpg'
import image2 from '../assets/images/image-product-2.jpg'
import image2th from '../assets/images/image-product-2-thumbnail.jpg'
import image3 from '../assets/images/image-product-3.jpg'
import image3th from '../assets/images/image-product-3-thumbnail.jpg'
import image4 from '../assets/images/image-product-4.jpg'
import image4th from '../assets/images/image-product-4-thumbnail.jpg'


const Hero = (props) => {

  // const [count, setCount] = useState(0)
  const [current ,setCurrent] = useState(0)
  const [current2 ,setCurrent2] = useState(0)

  let slides=[image1th, image2th, image3th, image4th]
  let ProductPics = [image1, image2, image3, image4]

  const next =()=>{
    let nextSlide = current == ProductPics.length-1 ? 0 : current +1 
    setCurrent(nextSlide)
  }
  const prev =()=>{
    let prevSlide = current == 0 ? ProductPics.length-1 : current -1 
    setCurrent(prevSlide)
  }
  const next2 =()=>{
    let nextSlide = current2 == ProductPics.length-1 ? 0 : current2 +1 
    setCurrent2(nextSlide)
  }
  const prev2 =()=>{
    let prevSlide = current2 == 0 ? ProductPics.length-1 : current2 -1 
    setCurrent2(prevSlide)
  }


  const element = slides.map((slide,index) =>
      <div key={index} className={index==current? 'border-2 border-opacity-100 border-orange-600 rounded-xl w-1/5 ' : 'border-none w-1/5 ' }>
          <img 
          src={slide} 
          onClick={()=>setCurrent(index)}
          className={index==current? 'opacity-40 rounded-xl' : 'opacity-100 rounded-xl' } 
           />
      </div>)

const element2 = slides.map((slide,index) =>
<div key={index} className={index==current2? 'border-2 border-opacity-100 border-orange-600 rounded-xl w-20 ' : 'border-none w-20 ' }>
    <img 
    src={slide} 
    onClick={()=>setCurrent2(index)}
    className={index==current2? 'opacity-40 rounded-xl w-20' : 'opacity-100 rounded-xl w-20' } 
     />
</div>)

  return (

        <div className="container flex  flex-col lg:flex lg:flex-row items-center justify-evenly mx-auto lg:space-y-10 lg:mt-10 lg:m-6">
            <div className='h-[400px] w-full lg:w-[30%] '>
              <label htmlFor="my-modal" className="hidden lg:block btn w-full h-full p-0 bg-transparent border-none hover:border-none hover:bg-transparent">
                <img src={ProductPics[current]} className='relative lg:rounded-xl h-[100%] w-[100%]' />
              </label> 
              <div className='hidden lg:flex lg:flex-wrap justify-between items-center my-6'>
                {element}
              </div>
              {/* arrows for mobile screen */}
              <img src={ProductPics[current]} className='relative lg:hidden lg:rounded-xl h-[100%] w-[100%]' />
              <div onClick={prev} className='absolute block lg:hidden top-[40%] translate-x-0 translate-y-5 left-5  bg-gray-200 rounded-full hover:bg-slate-400'>
                <BiChevronLeft size={30} />
              </div>
              <div onClick={next} className='absolute block lg:hidden top-[40%] translate-x-0 translate-y-5 right-5 bg-gray-200 rounded-full hover:bg-slate-400'>
                <BiChevronRight size={30} />
              </div>

            </div>
            {/* right side */}
            <div className="flex flex-col space-y-6 lg:w-[35%] px-8 py-6 lg:py-0 lg:px-0">
                {/* collection title */}
                <p className=' uppercase font-bold text-orange-500'>sneaker company</p>
                {/* product title */}
                <h2 className="card-title text-4xl font-bold">Full Limited Edition Sneakers</h2>
                {/* porduct description */}
                <p className='text-gray-400'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia error porro accusantium eveniet vitae delectus ipsa asperiores itaque impedit, iusto ullam debitis voluptatem repellendus reprehenderit enim assumenda quasi deleniti hic.</p>
                {/* price */}
                <div className='flex items-center justify-between lg:flex-col lg:items-start'>
                  <div className='flex items-center space-x-2'>
                    <h2 className='font-bold text-2xl'>$125.00</h2>
                    <p className='badge badge-sm border-none font-extrabold  text-orange-600 bg-orange-200'>50%</p>
                  </div>
                  <p className='line-through text-gray-300 font-bold'>$250.00</p>
                </div>
                
                {/* quantity and add to cart */}
                <div className="flex flex-col lg:flex-row space-y-2 lg:space-y-0 justify-start lg:space-x-2">
                <div className='flex items-center justify-between space-x-1 lg:w-1/3 bg-slate-200 rounded-xl'>
                  <button className={props.count <= 0 ? 'btn btn-disabled bg-transparent hover:bg-transparent':'btn btn-ghost bg-transparent hover:bg-transparent text-orange-500'} onClick={props.DecreaseCount}><AiOutlineMinus size={25}  /></button>
                  <div className='text-2xl font-bold'>
                    {props.count}
                  </div>
                  <button className='btn btn-ghost text-orange-500 bg-transparent hover:bg-transparent' onClick={props.IecreaseCount}><AiOutlinePlus size={25} /></button>
                </div>  
                <button htmlFor="my-modal" className="btn space-x-2 lg:w-2/3 bg-orange-500 text-white border-none drop-shadow-2xl hover:bg-orange-300">
                  <AiOutlineShoppingCart size={20} />
                  <h2>Add to cart</h2>
                </button>
                </div>
            </div>
          
           
            <input type="checkbox" id="my-modal" className="modal-toggle " />
                    <div className="hidden lg:modal bg-black/40" >
                        <div className="w-1/3 overflow-hidden">
                          <label htmlFor="my-modal" className="btn btn-sm btn-circle absolute right-[450px] top-2">✕</label>
                          <div className='flex flex-col '>
                            <img src={ProductPics[current2]} className='relative lg:rounded-xl w-[90%] mx-auto' />                            
                            <div onClick={prev2} className='absolute block top-[35%] translate-x-[440px] translate-y-5 left-5  bg-gray-200 rounded-full hover:bg-slate-400'>
                              <BiChevronLeft size={30} />
                            </div>
                            <div onClick={next2} className='absolute block  top-[35%] translate-x-[-440px] translate-y-5 right-5 bg-gray-200 rounded-full hover:bg-slate-400'>
                              <BiChevronRight size={30} />
                            </div>
                            <div className='flex flex-wrap justify-evenly items-center my-6'>
                              {element2}
                            </div>
                          </div>
                        </div>
                    </div>
        </div>
  
  )
}

export default Hero
