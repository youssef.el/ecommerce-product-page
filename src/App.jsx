import React,{ useState } from 'react'
import Header from './components/Header'
import Hero from './components/Hero'

const App = () => {
  const [count, setCount] = useState(0)

  function IncreaseCount(){
    setCount(prev => prev+1)
  }
  function DecreaseCount(){
    setCount(prev => prev-1)
  }
  function ClearCount(){
    setCount(0)
  }

  return (
    <div>
      <Header cartItems={count} ClearCount={ClearCount}/>
      <Hero  count={count} IecreaseCount={IncreaseCount} DecreaseCount={DecreaseCount}/>
    </div>
  )
}

export default App
